﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardsChildren : MonoBehaviour {


    public List<GameObject> MyChildrenCards = new List<GameObject>();
    [HideInInspector]
    public CardsChildren Instance;

    private void Awake()
    {
        Instance = this;
    }

    private void OnEnable()
    {
        DragCard.OnDropCard += DragCard_OnDropCard;
    }
    private void OnDisable()
    {
        DragCard.OnDropCard -= DragCard_OnDropCard;
    }

    private void DragCard_OnDropCard(GameObject _objDragged, GameObject _cardBelow, Vector3 _initialPos, Transform _initialParent, ObjectInfo _objectInfo, GameObject _currentColumn)
    {

        if(this.gameObject == _cardBelow)
        {
            MyChildrenCards.Add(_objDragged);

            List<GameObject> children = _objDragged.GetComponent<CardsChildren>().Instance.MyChildrenCards;
            int numChildren = children.Count;

            if (numChildren == 0)
                return;

            for (int i = 0; i < numChildren; i++)
            {
                GameObject child = children[i];
                if (!MyChildrenCards.Contains(child))
                    MyChildrenCards.Add(child);
            }


        }
        else
        {


            if (MyChildrenCards.Contains(_objDragged))
                MyChildrenCards.Remove(_objDragged);

            if (this.gameObject == _objDragged)
                return;

            List<GameObject> children = _objDragged.GetComponent<CardsChildren>().Instance.MyChildrenCards;
            int numChildren = children.Count;

            if (numChildren == 0)
                return;

            for (int i = 0; i < numChildren; i++)
            {
                GameObject child = children[i];
                if (MyChildrenCards.Contains(child))
                    MyChildrenCards.Remove(child);
            }
        }
    }


}
