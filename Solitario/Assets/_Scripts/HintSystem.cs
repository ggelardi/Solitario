﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class HintSystem : MonoBehaviour
{

    public List<GameObject> AvailableCards = new List<GameObject>();
    public Transform DepositoCuori;
    public Transform DepositoQuadri;
    public Transform DepositoFiori;
    public Transform DepositoPicche;


    //public List<GameObject> Columns = new List<GameObject>();

    //// Contiene info su quante carte sono presenti nella colonna e qual'è l'ultima carta presente
    //public List<KeyValuePair<GameObject, int>> ElementsInColumns = new List<KeyValuePair<GameObject, int>>();

    //// Use this for initialization
    //void Start()
    //{
    //    Invoke("GetAvailableCards", 0.2f);
    //}

    //public void GetAvailableCards()
    //{
    //    for (int i = 0; i < Columns.Count; i++)
    //    {
    //        GameObject column = Columns[i];

    //        List<GameObject> cardsInColumn = column.GetComponent<ColumnID>().CardsInColumn;
    //        int numCardsInColumn = cardsInColumn.Count;
    //        GameObject lastElementInColumn = cardsInColumn[numCardsInColumn - 1];

    //        ElementsInColumns[i] = new KeyValuePair<GameObject, int>(lastElementInColumn, numCardsInColumn);
    //    }

    //}
    // Use this for initialization

    private void OnEnable()
    {
        DeckShuffler.OnAllCardsPlaced += DeckShuffler_OnAllCardsPlaced;

    }

    private void DeckShuffler_OnAllCardsPlaced()
    {
        Invoke("GetAvailableCards", 0.2f);
    }

    private void OnDisable()
    {
        DeckShuffler.OnAllCardsPlaced -= DeckShuffler_OnAllCardsPlaced;

    }

    public void GetAvailableCards()
    {
        GameObject[] allCards = GameObject.FindGameObjectsWithTag("Card");

        foreach (var card in allCards)
        {
            if (card.GetComponent<CardID>().FaceUp)
                AvailableCards.Add(card);
        }
    }



    public void LookForHint()
    {
        // Andando per ordine tra tutte le carte disponibili controlliamo:
        // - C'è un asso? se si, portalo nel deposito
        // - c'è un un 2? C'è l'asso dello stesso seme nel deposito? se si portalo in deposito altrimenti
        // controlla se c'è un 3 di colore opposto
        int numAvailableCards = AvailableCards.Count;
        for (int i = 0; i < numAvailableCards; i++)
        {
            // Cache card
            CardID card = AvailableCards[i].GetComponent<CardID>();

            if (card.CardNumber == 1)
            {
                GameObject dummy = Instantiate(card.gameObject, card.gameObject.transform);
                dummy.transform.localPosition = Vector3.zero;
                dummy.transform.SetParent(null);

                if (card.CardSd == CardSeed.Cuori)
                    dummy.transform.DOMove(DepositoCuori.position, 1);
                else if (card.CardSd == CardSeed.Quadri)
                    dummy.transform.DOMove(DepositoQuadri.position, 1);
                else if (card.CardSd == CardSeed.Fiori)
                    dummy.transform.DOMove(DepositoFiori.position, 1);
                else if (card.CardSd == CardSeed.Picche)
                    dummy.transform.DOMove(DepositoPicche.position, 1);

                FadeColor(dummy);
                break;
            }

            else if (card.CardNumber > 1)
            {
                GameObject dummy = Instantiate(card.gameObject, card.gameObject.transform);
                dummy.transform.localPosition = Vector3.zero;
                dummy.transform.SetParent(null);

                for (int j = 0; j < numAvailableCards; j++)
                {
                    CardID greaterCard = AvailableCards[j].GetComponent<CardID>();

                    if (card.CardNumber == greaterCard.CardNumber - 1 && greaterCard.CardClr != card.CardClr)
                    {
                        dummy.transform.DOMove(greaterCard.gameObject.transform.position, 1);
                        FadeColor(dummy);
                        return;
                    }
                }
            }
        }

        DragCard.MovementComplete = true;

    }

    private void FadeColor(GameObject _go)
    {
        int childCount = _go.transform.childCount;
        if (childCount == 0)
            return;

        SpriteRenderer[] sprites = _go.GetComponentsInChildren<SpriteRenderer>();
        if (sprites.Length == 0)
            return;

        for (int i = 0; i < sprites.Length; i++)
        {
            sprites[i].DOFade(0, 1f);

        }

        Destroy(_go,1f);
    }
}
