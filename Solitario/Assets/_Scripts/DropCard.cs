﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropCard : MonoBehaviour
{

    private bool resetCard;

    // Use this for initialization
    void Start()
    {
        resetCard = true;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnEnable()
    {
        DragCard.OnDropCard += DragCard_OnDropCard;
    }

    private void DragCard_OnDropCard(GameObject _objDragged, GameObject _cardBelow, Vector3 _initialPos, Transform _initilaParent, ObjectInfo _objectInfo, GameObject _initialColumn)
    {
        resetCard = true;
        DropOnCard(_objDragged, _cardBelow, _initialPos, _initilaParent, _objectInfo, _initialColumn);
    }

    private void DropOnCard(GameObject _objDragged, GameObject _cardBelow, Vector3 _initialPos, Transform _initilaParent, ObjectInfo _objectInfo, GameObject _initialColumn)
    {
        CardID card = _cardBelow.GetComponent<CardID>();
        CardColor cardColor = card.CardClr;
        CardSeed cardSeed = card.CardSd;
        int cardNum = card.CardNumber;

        CardID myCard = _objDragged.GetComponent<CardID>();
        CardColor myColor = myCard.CardClr;
        CardSeed myCardSeed = myCard.CardSd;
        int myNum = myCard.CardNumber;

        // Vogliamo controllare se:
        // - Il numero della carta che stiamo lasciando è inferiore a quello della carta sottostante
        // - La carta che abbiamo è un asso dello stesso seme di quello dove lo stiamo per depositare
        // - Stiamo mettendo la carta su un Placeholder (sul tavolo)
        if ((myNum == cardNum - 1 && myColor != cardColor) || (_cardBelow.tag == "Placeholder"))
        {
            _objDragged.transform.SetParent(_cardBelow.transform);
            if (_cardBelow.tag == "Placeholder")
                _objDragged.transform.localPosition = Vector3.zero;
            else
                _objDragged.transform.localPosition = Vector3.zero + Vector3.up * -0.5f;

            _objDragged.GetComponent<BoxCollider>().enabled = true;
            StartCoroutine(CheckCardsParent(_objDragged, _initialColumn));

            DragCard.MovementComplete = true;
            resetCard = false;

        }

        // Se stiamo cercando di mettere la carta nel container finale assicuriamoci
        // che la carta che abbiamo è superiore a quella già contenuta 
        if ((cardNum == 99 && myCardSeed == cardSeed && myNum == 1) || (myNum == cardNum + 1 && myCardSeed == cardSeed))
        {
            _objDragged.transform.SetParent(_cardBelow.transform);
            _objDragged.transform.localPosition = Vector3.zero;
            // diabilitiamo il box collider così che la carta non potrà più essere rimossa
            _objDragged.GetComponent<BoxCollider>().enabled = false;

            StartCoroutine(CheckCardsParent(_objDragged, _initialColumn));
            DragCard.MovementComplete = true;
            resetCard = false;
        }

        if (resetCard)
            DragCard.ResetCard(_objDragged, _initialPos, _initilaParent);
    }


    IEnumerator CheckCardsParent(GameObject _card, GameObject _previousColumn)
    {
        yield return new WaitForSeconds(0.2f);


        if (_previousColumn == null)
            yield break;

        if (_previousColumn.GetComponent<ColumnID>() == null)
            yield break;

        List<GameObject> cardsChildren = _card.GetComponent<CardsChildren>().Instance.MyChildrenCards;
        int numChildren = cardsChildren.Count;

        if (_previousColumn.GetComponent<ColumnID>().CardsInColumn.Contains(_card))
            _previousColumn.GetComponent<ColumnID>().CardsInColumn.Remove(_card);

        for (int i = 0; i < numChildren; i++)
        {
            if (_previousColumn.GetComponent<ColumnID>().CardsInColumn.Contains(cardsChildren[i]))
                _previousColumn.GetComponent<ColumnID>().CardsInColumn.Remove(cardsChildren[i]);
        }

        Transform rootParent = _card.transform.root;
        if (rootParent.GetComponent<ColumnID>() == null)
            yield break;
        rootParent.GetComponent<ColumnID>().CardsInColumn.Add(_card);

        for (int i = 0; i < numChildren; i++)
            rootParent.GetComponent<ColumnID>().CardsInColumn.Add(cardsChildren[i]);
    }
}
