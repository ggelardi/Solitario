﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public Text Mosse;
    public Text Punti;

    private int mosseCounter;
    private int puntiCounter;
    private GameObject draggedObj;
    private GameObject onDropObj;
    private GameObject draggedObj_parent;
    private Vector3 draggedObj_initialPos;

    private int draggedObj_num;
    private CardColor draggedObj_clr;
    private bool ignoreDragExit = false;
    // Use this for initialization
    void Start()
    {
        ignoreDragExit = false;
        mosseCounter = 0;
        puntiCounter = 0;
    }

    private void OnEnable()
    {
        DropHere.OnObjectDropped += DropHere_OnObjectDropped;
        Draggable.OnObjDragged += Draggable_OnObjDragged;
        Draggable.OnObjReleased += Draggable_OnObjReleased;
    }

    private void Draggable_OnObjReleased(GameObject _draggedObj, Vector3 _initialPosition, GameObject _originalParent)
    {
        if (ignoreDragExit)
        {
            ignoreDragExit = false;
            return;
        }

        ResetPosition();
    }

    private void OnDisable()
    {
        DropHere.OnObjectDropped -= DropHere_OnObjectDropped;
        Draggable.OnObjDragged -= Draggable_OnObjDragged;
        Draggable.OnObjReleased -= Draggable_OnObjReleased;

    }


    private void Draggable_OnObjDragged(GameObject _draggedObj, Vector3 _initialPosition, GameObject _originalParent)
    {
        draggedObj = _draggedObj;
        draggedObj_initialPos = _initialPosition;
        draggedObj_parent = _originalParent;

        CardAppearance card = draggedObj.GetComponent<CardAppearance>();
        draggedObj_num = card.CardNumber;
        draggedObj_clr = card.CardClr;
    }

    private void DropHere_OnObjectDropped(GameObject _go)
    {
        print(_go.name);


        if (_go.transform.parent.tag == "Placeholder")
        {
            CardSeed placeholderType = _go.transform.parent.GetComponent<PlaceholdersTypes>().CardType;
            if (placeholderType == draggedObj.GetComponent<CardAppearance>().CardSd || placeholderType == CardSeed.None)
            {
                draggedObj.transform.SetParent(_go.transform.parent.GetChild(2).transform);
                ignoreDragExit = true;
                mosseCounter++;
                puntiCounter++;
                Mosse.text = "Mosse\n" + mosseCounter;
                return;
            }

            return;
        }
        onDropObj = _go.transform.parent.gameObject;

        CardAppearance card = onDropObj.GetComponent<CardAppearance>();
        int cardNumber = card.CardNumber;
        CardColor cardColor = card.CardClr;

        if (draggedObj_num == cardNumber-1 && cardColor != draggedObj_clr)
        {
            draggedObj.transform.SetParent(onDropObj.transform.GetChild(2).transform);
            mosseCounter++;
            Mosse.text = "Mosse\n" + mosseCounter; 
        }
        else
        {

            ResetPosition();
        }
        ignoreDragExit = true;

    }


    private void ResetPosition()
    {
        draggedObj.transform.DOMove(draggedObj_initialPos, 1);
        draggedObj.transform.SetParent(draggedObj_parent.transform);

    }


    // Update is called once per frame
    void Update()
    {

    }
}
