﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardID : MonoBehaviour {

    public Card CardObject;
    public Material Black;
    public Material Red;
    [HideInInspector]
    public CardColor CardClr;
    [HideInInspector]
    public CardSeed CardSd;
    public int CardNumber;
    [HideInInspector]
    public CardID Instance;

    private SpriteRenderer numImage;
    private SpriteRenderer seedImage;
    private SpriteRenderer centerImage;

    public bool FaceUp;

    public delegate void CardIDEventHandler();


    private void Awake()
    {
        Instance = this;
    }

    private void OnEnable()
    {
        DeckShuffler.OnDeckInitialized += DeckShuffler_OnDeckInitialized;
    }
    private void OnDisable()
    {
        DeckShuffler.OnDeckInitialized -= DeckShuffler_OnDeckInitialized;

    }
    private void DeckShuffler_OnDeckInitialized()
    {

    }


    public void InitializeCard()
    {
        numImage = this.transform.GetChild(0).GetChild(0).GetComponent<SpriteRenderer>();
        numImage.sprite = CardObject.NumImage;
        seedImage = this.transform.GetChild(0).GetChild(1).GetComponent<SpriteRenderer>();
        seedImage.sprite = CardObject.SeedImage;
        centerImage = this.transform.GetChild(0).GetChild(2).GetComponent<SpriteRenderer>();
        centerImage.sprite = CardObject.CenterImage;

        CardNumber = CardObject.Number;
        CardSd = CardObject.Seed;

        if (CardObject.Seed == CardSeed.Cuori || CardObject.Seed == CardSeed.Quadri)
        {
            numImage.material = Red;
            CardClr = CardColor.Red;
        }
        else
        {
            numImage.material = Black;
            CardClr = CardColor.Black;
        }
    }


    public void GetFacingDirection()
    {
        if (this.transform.localEulerAngles.y == 0)
            FaceUp = true;
        else
            FaceUp = false;

        if (FaceUp)
            this.GetComponent<CardLayerManager>().Instance.SetCardLayer(this.gameObject, CardLayerManager.DEFAULT_LAYER);
        else
            this.GetComponent<CardLayerManager>().Instance.SetCardLayer(this.gameObject, CardLayerManager.BACK);
    }
}
