﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardLayerManager : MonoBehaviour
{


    public const string ON_PICKED_LAYER = "Picked";
    public const string DEFAULT_LAYER = "Default";
    public const string LAST_CARD = "LastCard";
    public const string BACK = "Back";

    private const int MAX_INDEX_IN_LAYER = 26;
    [HideInInspector]
    public CardLayerManager Instance;
    private int myNumber;
    private SpriteRenderer[] frontAndBack = new SpriteRenderer[2];
    private SpriteRenderer[] images = new SpriteRenderer[3];

    private void Awake()
    {
        Instance = this;
    }

    // Use this for initialization
    void Start()
    {
        Invoke("SortLayers", 0.2f);
    }

    private void OnEnable()
    {
        DragCard.OnStartDragging += DragCard_OnStartDragging;
        DragCard.OnStopDragging += DragCard_OnStopDragging;
    }

    private void OnDisable()
    {
        DragCard.OnStartDragging -= DragCard_OnStartDragging;
        DragCard.OnStopDragging -= DragCard_OnStopDragging;
    }

    private void DragCard_OnStartDragging(GameObject _objDragged)
    {
        if (_objDragged != this.gameObject)
            SetCardLayer(this.gameObject, DEFAULT_LAYER);
        else
            SetCardLayer(_objDragged, ON_PICKED_LAYER);

    }

    private void DragCard_OnStopDragging(GameObject _objDragged)
    {
        SetCardLayer(_objDragged, DEFAULT_LAYER);
    }
    public void SetCardLayer(GameObject _card, string _layer)
    {
        int childCount = _card.transform.childCount;
        if (childCount == 0)
            return;

        SpriteRenderer[] sprites = _card.GetComponentsInChildren<SpriteRenderer>();
        if (sprites.Length == 0)
            return;

        for (int i = 0; i < sprites.Length; i++)
        {
            sprites[i].sortingLayerName = _layer;

        }
        SortIndex(_card, _layer);
    }

    private void SortLayers()
    {

        myNumber = this.GetComponent<CardID>().CardNumber;
        frontAndBack[0] = this.transform.GetChild(0).GetComponent<SpriteRenderer>();
        frontAndBack[1] = this.transform.GetChild(1).GetComponent<SpriteRenderer>();
        images[0] = this.transform.GetChild(0).GetChild(0).GetComponent<SpriteRenderer>();
        images[1] = this.transform.GetChild(0).GetChild(1).GetComponent<SpriteRenderer>();
        images[2] = this.transform.GetChild(0).GetChild(2).GetComponent<SpriteRenderer>();

        frontAndBack[0].sortingOrder = -MAX_INDEX_IN_LAYER * myNumber;
        frontAndBack[1].sortingOrder = -MAX_INDEX_IN_LAYER * myNumber;

        for (int i = 0; i < images.Length; i++)
            images[i].sortingOrder = -MAX_INDEX_IN_LAYER * myNumber + 1;
    }

    private void SortIndex(GameObject _card, string _layer)
    {
        myNumber = _card.GetComponent<CardID>().CardNumber;
        frontAndBack[0] = _card.transform.GetChild(0).GetComponent<SpriteRenderer>();
        frontAndBack[1] = _card.transform.GetChild(1).GetComponent<SpriteRenderer>();
        images[0] = _card.transform.GetChild(0).GetChild(0).GetComponent<SpriteRenderer>();
        images[1] = _card.transform.GetChild(0).GetChild(1).GetComponent<SpriteRenderer>();
        images[2] = _card.transform.GetChild(0).GetChild(2).GetComponent<SpriteRenderer>();

        if(_layer == BACK)
        {
            frontAndBack[0].sortingOrder = 0;
            frontAndBack[1].sortingOrder = 0;

            for (int i = 0; i < images.Length; i++)
                images[i].sortingOrder = 01;
        }

        frontAndBack[0].sortingOrder = -MAX_INDEX_IN_LAYER * myNumber;
        frontAndBack[1].sortingOrder = -MAX_INDEX_IN_LAYER * myNumber;

        for (int i = 0; i < images.Length; i++)
            images[i].sortingOrder = -MAX_INDEX_IN_LAYER * myNumber + 1;
    }
}
