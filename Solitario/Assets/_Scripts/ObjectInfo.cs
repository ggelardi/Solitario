﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectInfo : MonoBehaviour {

    [HideInInspector]
    public ObjectInfo Instance;
    [HideInInspector]
    public Vector3 CurrentPos;
    [HideInInspector]
    public Transform CurrentParent;


    private void Awake()
    {
        Instance = this;
    }

    private void OnEnable()
    {
        DeckShuffler.OnAllCardsPlaced += DeckShuffler_OnAllCardsPlaced;
    }

    private void OnDisable()
    {
        DeckShuffler.OnAllCardsPlaced -= DeckShuffler_OnAllCardsPlaced;
    }
    private void DeckShuffler_OnAllCardsPlaced()
    {
        CurrentParent = this.transform.parent;
        CurrentPos = this.transform.position;
    }

}
