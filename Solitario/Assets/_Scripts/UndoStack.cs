﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UndoStack : MonoBehaviour
{

    public static List<KeyValuePair<GameObject, ObjectInfo>> StackList = new List<KeyValuePair<GameObject, ObjectInfo>>();


    private void OnEnable()
    {
        DragCard.OnDropCard += DragCard_OnDropCard;
    }

    private void DragCard_OnDropCard(GameObject _objDragged, GameObject _cardBelow, Vector3 _initialPos, Transform _initialParent, ObjectInfo _objectInfo, GameObject _initialColumn)
    {
        StackList.Add(new KeyValuePair<GameObject, ObjectInfo>(_objDragged, _objectInfo));
    }

    private void OnDisable()
    {
        DragCard.OnDropCard -= DragCard_OnDropCard;
    }

    public void UndoAction()
    {
        DragCard.MovementComplete = true;
        int listCount = StackList.Count;

        if (listCount == 0)
            return;

        GameObject go = StackList[listCount - 1].Key;
        go.transform.SetParent(null);
        go.transform.position = StackList[listCount - 1].Value.Instance.CurrentPos;
        go.transform.SetParent(StackList[listCount - 1].Value.Instance.CurrentParent);
        go.GetComponent<BoxCollider>().enabled = true;
        StackList.RemoveAt(listCount - 1);

    }
}
