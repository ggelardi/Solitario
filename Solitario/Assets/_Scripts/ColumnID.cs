﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ColumnNumber
{
    One,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    None
}

public class ColumnID : MonoBehaviour
{

    public ColumnNumber ColumnNumber;
    public List<GameObject> CardsInColumn = new List<GameObject>();

    private void OnEnable()
    {
        DragCard.OnDropCard += DragCard_OnDropCard;
    }
    private void OnDisable()
    {
        DragCard.OnDropCard -= DragCard_OnDropCard;
    }

    private void DragCard_OnDropCard(GameObject _objDragged, GameObject _cardBelow, Vector3 _initialPos, Transform _initialParent, ObjectInfo _objectInfo, GameObject _columnNumber)
    {
      //  Invoke("NumOfCardsInColumn", 0.2f);
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void NumOfCardsInColumn()
    {
        CardsInColumn.Clear();
        foreach (Transform child in this.transform)
        {
            if (child.CompareTag("Card"))
            {
                CardsInColumn.Add(child.transform.gameObject);
            }
        }

    }
}
