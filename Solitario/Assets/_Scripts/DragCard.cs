﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DragCard : MonoBehaviour
{

    public delegate void DragEventHandler(GameObject _objDragged);
    public static event DragEventHandler OnStartDragging;
    public static event DragEventHandler OnStopDragging;

    public delegate void DropEventHandler(GameObject _objDragged, GameObject _cardBelow, Vector3 _initialPos, Transform _initialParent, ObjectInfo _objectInfo, GameObject _currentColumn);
    public static event DropEventHandler OnDropCard;

    //Initialize Variables
    GameObject cardTarget;
    bool isMouseDragging;
    Vector3 offsetValue;
    Vector3 positionOfScreen;

    private Vector3 initialCardPos;
    private Transform initialParent;
    private GameObject initialColumn;
    public static bool MovementComplete = true;
    private bool ignoreClickUp = false;
    void Update()
    {
        print(MovementComplete);

        //Mouse Button Press Down
        if (Input.GetMouseButtonDown(0) && MovementComplete)
        {
            MovementComplete = false;
            RaycastHit hitInfo;
            cardTarget = RaycastCards(out hitInfo);

            if (cardTarget == null)
            {
                MovementComplete = true;
                return;
            }

            if (!CardIsInteractable(cardTarget))
            {
                MovementComplete = true;
                ignoreClickUp = true;
                return;
            }

            int cardNum = cardTarget.GetComponent<CardID>().CardNumber;
            if (cardTarget.tag == "Placeholder" || cardNum == 99)
                return;

            initialCardPos = cardTarget.transform.position;
            initialParent = cardTarget.transform.parent;

            if (cardTarget.transform.parent == null)
                initialColumn = null;
            else
            {
                initialColumn = cardTarget.transform.root.gameObject;

            }



            cardTarget.transform.SetParent(null);
            isMouseDragging = true;


            //Converting world position to screen position.
            positionOfScreen = Camera.main.WorldToScreenPoint(cardTarget.transform.position);
            offsetValue = cardTarget.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, positionOfScreen.z));

            if (OnStartDragging != null)
                OnStartDragging(cardTarget);

        }

        //Mouse Button Up
        if (Input.GetMouseButtonUp(0))
        {
            isMouseDragging = false;

            if (cardTarget == null)
                return;

            if (ignoreClickUp)
            {
                ignoreClickUp = false;
                return;
            }

            RaycastHit hitInfo;
            GameObject cardBelow = RaycastCards(out hitInfo);

            if (cardBelow != null)
            {
                ObjectInfo objectInfo = cardTarget.GetComponent<ObjectInfo>();
                objectInfo.Instance.CurrentParent = initialParent;
                objectInfo.Instance.CurrentPos = initialCardPos;



                if (OnDropCard != null)
                    OnDropCard(cardTarget, cardBelow, initialCardPos, initialParent, objectInfo, initialColumn);
            }
            else
            {
                int cardNum = cardTarget.GetComponent<CardID>().CardNumber;
                if (cardTarget.tag == "Placeholder" || cardNum == 99)
                    return;

                ResetCard(cardTarget, initialCardPos, initialParent);

            }

            if (OnStopDragging != null)
                OnStopDragging(cardTarget);

            cardTarget = null;
        }

        //Is mouse Moving
        if (isMouseDragging)
        {

            //tracking mouse position.
            Vector3 currentScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, positionOfScreen.z);

            //converting screen position to world position with offset changes.
            Vector3 currentPosition = Camera.main.ScreenToWorldPoint(currentScreenSpace) + offsetValue;

            //It will update target gameobject's current postion.
            cardTarget.transform.position = currentPosition;

            cardTarget.GetComponent<BoxCollider>().enabled = false;
        }


    }

    //Method to Return Clicked Object
    GameObject RaycastCards(out RaycastHit hit)
    {
        GameObject target = null;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray.origin, ray.direction * 10, out hit))
        {
            target = hit.collider.gameObject;
        }
        return target;
    }

    public static void ResetCard(GameObject _card, Vector3 _initialPos, Transform _initialParent)
    {
        int cardNum = _card.GetComponent<CardID>().CardNumber;
        if (_card.tag == "Placeholder" || cardNum == 99)
        {
            MovementComplete = true;
            return;
        }

        _card.transform.DOMove(_initialPos, 1).OnComplete(() =>
        {
            //Alla fine del lerping vogliamo garantire che la carta sia ben posizionata (lerping non è mai 100% preciso)
            _card.transform.position = _initialPos;
            MovementComplete = true;
        });
        _card.transform.SetParent(_initialParent);
        _card.GetComponent<BoxCollider>().enabled = true;
    }

    private bool CardIsInteractable(GameObject _card)
    {
        bool isFacingUp = _card.GetComponent<CardID>().FaceUp;
        int numChildren = _card.GetComponent<CardsChildren>().Instance.MyChildrenCards.Count;

        if (isFacingUp)
            return true;
        else
        {
            if (numChildren > 0)
                return false;

            _card.transform.DOLocalRotate(Vector3.zero, 0.5f);
            _card.GetComponent<CardID>().Instance.GetFacingDirection();
            return true;

        }
    }
}
