﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum CardColor
{
    Black,
    Red
}

public class CardAppearance : MonoBehaviour {

    public Card CardObject;
    public Material Black;
    public Material Red;
    [HideInInspector]
    public CardColor CardClr;
    [HideInInspector]
    public CardSeed CardSd;
    [HideInInspector]
    public int CardNumber;
    [HideInInspector]
    public CardAppearance Instance;

    private Image numImage;
    private Image seedImage;
    private Image centerImage;

    private void Awake()
    {
        Instance = this;

    }

    // Use this for initialization
    void Start () {

        numImage = this.transform.GetChild(0).GetChild(0).GetComponent<Image>();
        numImage.sprite = CardObject.NumImage;
        seedImage = this.transform.GetChild(0).GetChild(1).GetComponent<Image>();
        seedImage.sprite = CardObject.SeedImage;
        centerImage = this.transform.GetChild(0).GetChild(2).GetComponent<Image>();
        centerImage.sprite = CardObject.CenterImage;

        CardNumber = CardObject.Number;
        CardSd = CardObject.Seed;

        if (CardObject.Seed == CardSeed.Cuori || CardObject.Seed == CardSeed.Quadri)
        {
            numImage.material = Red;
            CardClr = CardColor.Red;
        }
        else
        {
            numImage.material = Black;
            CardClr = CardColor.Black;
        }

    }

    // Update is called once per frame
    void Update () {
		
	}
}
