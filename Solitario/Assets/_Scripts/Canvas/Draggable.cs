﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

public class Draggable : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    private RaycastHit hit;
    private Vector3 initialPosition;
    private GameObject originalParent;
    private int myIndex;

    public delegate void DragEventHandler(GameObject _draggedObj, Vector3 _initialPosition, GameObject _originalParent);
    public static event DragEventHandler OnObjDragged;
    public static event DragEventHandler OnObjReleased;


    private void Start()
    {
        myIndex = this.transform.GetSiblingIndex();
    }


    public void OnBeginDrag(PointerEventData eventData)
    {
        initialPosition = this.transform.position;
        this.transform.SetAsLastSibling();
        originalParent = this.transform.parent.gameObject;
        if (OnObjDragged != null)
            OnObjDragged(this.gameObject, initialPosition, originalParent);
    }

    public void OnDrag(PointerEventData eventData)
    {
        this.transform.position = eventData.position;
        GetComponent<CanvasGroup>().blocksRaycasts = false;

    }

    public void OnEndDrag(PointerEventData eventData)
    {
        this.transform.SetSiblingIndex(myIndex);
        GetComponent<CanvasGroup>().blocksRaycasts = true;

        if (OnObjReleased != null)
            OnObjReleased(this.gameObject, initialPosition, originalParent);

    }
}
