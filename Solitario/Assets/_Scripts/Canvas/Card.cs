﻿using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New Card", menuName = "Cards")]
public class Card : ScriptableObject {

    public int Number;
    public CardSeed Seed;

    public Sprite SeedImage;
    public Sprite NumImage;
    public Sprite CenterImage;
}
