﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DropHere : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{
    public delegate void DropEventHandler(GameObject _go);
    public static event DropEventHandler OnObjectDropped;


    public void OnDrop(PointerEventData eventData)
    {
        //print(" OnDrop" + eventData.hovered[0].transform.parent.name);

        if (OnObjectDropped != null)
            OnObjectDropped(eventData.hovered[0]);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        print("OnEnter ");

    }

    public void OnPointerExit(PointerEventData eventData)
    {
        print("OnExit");

    }
}
