﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;

public class DeckShuffler : MonoBehaviour
{
    public List<Card> Cards = new List<Card>();
    public GameObject CardPrefab;
    public GameObject[] Placeholders;
    public GameObject[] DepositiCarte;

    public Transform DeckButton;

    private List<GameObject> myCards = new List<GameObject>();

    public delegate void DeckEventHandler();
    public static event DeckEventHandler OnDeckInitialized;
    public static event DeckEventHandler OnAllCardsPlaced;

    private void OnEnable()
    {
        OnDeckInitialized += DeckShuffler_OnDeckInitialized;
    }



    private void OnDisable()
    {
        OnDeckInitialized -= DeckShuffler_OnDeckInitialized;
    }
    // Use this for initialization
    void Start()
    {
        Cards.ShuffleList();
        Cards.ShuffleList();

        for (int i = 0; i < Placeholders.Length; i++)
            Placeholders[i].GetComponent<CardID>().Instance.InitializeCard();
        for (int i = 0; i < DepositiCarte.Length; i++)
            DepositiCarte[i].GetComponent<CardID>().Instance.InitializeCard();

        for (int i = 0; i < Cards.Count; i++)
        {
            GameObject newCard = Instantiate(CardPrefab, DeckButton.transform) as GameObject;
            newCard.name = Cards[i].name;
            newCard.GetComponent<CardID>().Instance.CardObject = Cards[i];
            newCard.GetComponent<CardID>().Instance.InitializeCard();
            newCard.transform.localPosition = Vector3.zero;
            newCard.transform.SetParent(null);
            myCards.Add(newCard);
        }


        if (OnDeckInitialized != null)
            OnDeckInitialized();

    }


    private void DeckShuffler_OnDeckInitialized()
    {
        DistributeCards();
    }

    private void DistributeCards()
    {
        #region CARD 1
        SetupFirstRow(myCards[0], 0);
        #endregion

        #region CARD 2
        SetupFirstRow(myCards[1], 1);
        #endregion

        #region CARD 3
        SetupSecondRow(myCards[2], 2, 1);
        #endregion

        #region CARD 4
        SetupFirstRow(myCards[3], 2);
        #endregion

        #region CARD 5
        SetupSecondRow(myCards[4], 4, 2);
        #endregion

        #region CARD 6
        SetupRemainingRows(myCards[5], 5, 2, 2);
        #endregion

        #region CARD 7
        SetupFirstRow(myCards[6], 3);
        #endregion

        #region CARD 8
        SetupSecondRow(myCards[7], 7, 3);
        #endregion

        #region CARD 9
        SetupRemainingRows(myCards[8], 8, 3, 2);
        #endregion

        #region CARD 10
        SetupRemainingRows(myCards[9], 9, 3, 3);
        #endregion

        #region CARD 11
        SetupFirstRow(myCards[10], 4);
        #endregion

        #region CARD 12
        SetupSecondRow(myCards[11], 11, 4);
        #endregion

        #region CARD 13
        SetupRemainingRows(myCards[12], 12, 4, 2);
        #endregion

        #region CARD 14
        SetupRemainingRows(myCards[13], 13, 4, 3);
        #endregion

        #region CARD 15
        SetupRemainingRows(myCards[14], 14, 4, 4);
        #endregion

        #region CARD 16
        SetupFirstRow(myCards[15], 5);
        #endregion

        #region CARD 17
        SetupSecondRow(myCards[16], 16, 5);
        #endregion

        #region CARD 18
        SetupRemainingRows(myCards[17], 17, 5, 2);
        #endregion

        #region CARD 19
        SetupRemainingRows(myCards[18], 18, 5, 3);
        #endregion

        #region CARD 20
        SetupRemainingRows(myCards[19], 19, 5, 4);
        #endregion

        #region CARD 21
        SetupRemainingRows(myCards[20], 20, 5, 5);
        #endregion

        #region CARD 22
        SetupFirstRow(myCards[21], 6);
        #endregion

        #region CARD 23
        SetupSecondRow(myCards[22], 22, 6);
        #endregion

        #region CARD 24
        SetupRemainingRows(myCards[23], 23, 6, 2);
        #endregion
        
        #region CARD 25
        SetupRemainingRows(myCards[24], 24, 6, 3);
        #endregion

        #region CARD 26
        SetupRemainingRows(myCards[25], 25, 6, 4);
        #endregion

        #region CARD 27
        SetupRemainingRows(myCards[26], 26, 6, 5);
        #endregion

        #region CARD 28
        SetupRemainingRows(myCards[27], 27, 6, 6);
        #endregion


        if (OnAllCardsPlaced != null)
            OnAllCardsPlaced();
    }


    private void SetupFirstRow(GameObject _card, int _numPlaceholder)
    {
        if (_numPlaceholder > 0)
            _card.transform.localEulerAngles = Vector3.up * 180;

        _card.GetComponent<CardID>().GetFacingDirection();

        _card.transform.DOMove(Placeholders[_numPlaceholder].transform.position, 1).OnComplete(() =>
        {
            _card.transform.localPosition = Vector3.zero;

        });
        _card.transform.SetParent(Placeholders[_numPlaceholder].transform);

        Placeholders[_numPlaceholder].GetComponent<ColumnID>().CardsInColumn.Add(_card);
    }

    private void SetupSecondRow(GameObject _card, int _thisCardNum, int _numPlaceholder)
    {
        if(_numPlaceholder > 1)
            _card.transform.localEulerAngles = Vector3.up * 180;

        _card.GetComponent<CardID>().GetFacingDirection();

        _card.transform.DOMove(myCards[_thisCardNum - 1].transform.position + Vector3.up * -0.5f, 1).OnComplete(() =>
        {
            _card.transform.localPosition = Vector3.zero + Vector3.up * -0.5f;
        });
        _card.transform.SetParent(myCards[_thisCardNum - 1].transform);
        myCards[_thisCardNum - 1].GetComponent<CardsChildren>().Instance.MyChildrenCards.Add(_card);
        Placeholders[_numPlaceholder].GetComponent<ColumnID>().CardsInColumn.Add(_card);
    }

    private void SetupRemainingRows(GameObject _card, int _thisCardNum, int _numPlaceholder, int numParents)
    {
        if (numParents < _numPlaceholder)
            _card.transform.localEulerAngles = Vector3.up * 180;

        _card.GetComponent<CardID>().GetFacingDirection();


        _card.transform.DOMove(myCards[_thisCardNum - 1].transform.position + Vector3.up * -0.5f, 1).OnComplete(() =>
        {
            _card.transform.localPosition = Vector3.zero + Vector3.up * -0.5f;
        });
        _card.transform.SetParent(myCards[_thisCardNum - 1].transform);

        for (int i = 1; i < numParents + 1; i++)
            myCards[_thisCardNum - i].GetComponent<CardsChildren>().Instance.MyChildrenCards.Add(_card);

        Placeholders[_numPlaceholder].GetComponent<ColumnID>().CardsInColumn.Add(_card);
    }
}