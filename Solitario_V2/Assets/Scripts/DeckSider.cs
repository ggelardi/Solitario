﻿using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;

public class DeckSider : MonoBehaviour
{
    public int CardsAbailable = 0;
    public Transform DeckPos;
    public List<KeyValuePair<GameObject, float>> CardsInDeckSider = new List<KeyValuePair<GameObject, float>>();
    // Le posizioni che prenderanno le carte che non sono al primo posto nel mucchio
    private Vector3 pos1;
    private Vector3 pos2;


    private void Start()
    {
        pos1 = this.transform.position + Vector3.right * 0.25f;
        pos2 = this.transform.position;

    }

    public void ShiftCards()
    {
        // Controlla nuovamente le dimensioni della lista, better safe than sorry
        int listLength = CardsInDeckSider.Count;

        if (CardsAbailable > 2 && listLength > 2)
        {

            CardsInDeckSider[listLength - 1].Key.transform.DOMove(pos1, 0.5f);
            CardsInDeckSider[listLength - 1].Key.GetComponent<BoxCollider2D>().enabled = false;
            CardsInDeckSider[listLength - 1].Key.GetComponent<Card>().IsFirst = false;
            CardsInDeckSider[listLength - 2].Key.transform.DOMove(pos2, 0.5f);
            CardsInDeckSider[listLength - 2].Key.GetComponent<BoxCollider2D>().enabled = false;
            CardsInDeckSider[listLength - 2].Key.GetComponent<Card>().IsFirst = false;


        }
    }

    public void ResetLastCardCollider()
    {
        // Controlla nuovamente le dimensioni della lista, better safe than sorry
        int listLength = CardsInDeckSider.Count;

        print(listLength);
        if (listLength == 0)
            return;

        CardsInDeckSider[listLength - 1].Key.GetComponent<BoxCollider2D>().enabled = true;
        CardsInDeckSider[listLength - 1].Key.GetComponent<Card>().IsFirst = true;
        CardsInDeckSider[listLength - 1].Key.GetComponent<Card>().FacingUp = true;
        CardsInDeckSider[listLength - 1].Key.GetComponent<Card>().IsNextToDeck = true;
        CardsInDeckSider[listLength - 1].Key.GetComponent<Card>().OnTable = false;
        CardsInDeckSider[listLength - 1].Key.GetComponent<Card>().OnDeposit = false;
        CardsInDeckSider[listLength - 1].Key.GetComponent<Card>().IsInDeck = false;
    }

    public void ResetCardInDeck()
    {

        for (int i = 0; i < CardsInDeckSider.Count; i++)
        {
            // Cache card
            GameObject card = CardsInDeckSider[i].Key;

            if (i == 0)
                card.GetComponent<Card>().IsFirst = true;
            else
                card.GetComponent<Card>().IsFirst = false;

            card.GetComponent<Card>().FacingUp = false;
            card.GetComponent<Card>().IsNextToDeck = false;
            card.GetComponent<Card>().OnTable = false;
            card.GetComponent<Card>().OnDeposit = false;
            card.GetComponent<Card>().IsInDeck = true;
            card.GetComponent<BoxCollider2D>().enabled = true;
            card.GetComponent<Card>().SetupCardApperance();


            card.transform.position = new Vector3(DeckPos.position.x, DeckPos.position.y,CardsInDeckSider[i].Value);
            CardsAbailable--;
            DeckPos.gameObject.GetComponent<Deck>().AvailableCards++;
        }

        //CardsInDeckSider.Remove(CardsInDeckSider[i]);
        CardsInDeckSider.Clear();


    }

}
