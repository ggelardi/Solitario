﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HintSystem : MonoBehaviour
{
    public Transform[] Deposit = new Transform[4];
    // I blocchi vengono assegnati in CartCreator dopo che vengono creati
    public Transform[] TableBlock = new Transform[7];
    public List<GameObject> AllCards = new List<GameObject>();
    public List<Card> FacingUpCards = new List<Card>();
    public GameObject CoveredCard;
    public Transform DeckPos;
    public Transform DeckSiderPos;
    private bool hintFound;


    // Use this for initialization
    void Start()
    {
        FacingUpCards.Clear();
    }

    public void GetHint()
    {
        hintFound = false;
        FacingUpCards.Clear();
        int numCards = AllCards.Count;

        for (int i = 0; i < numCards; i++)
        {
            Card card = AllCards[i].GetComponent<Card>();

            if (card.FacingUp)
                FacingUpCards.Add(card);
        }


        int numFacingUpCards = FacingUpCards.Count;

        for (int i = 0; i < numFacingUpCards; i++)
        {
            if (hintFound)
                break;

            Card card = FacingUpCards[i];

            if (card.OnDeposit)
                continue;

            if(card.Number == 1 && card.IsFirst)
            {
                // Valore seme (quadri = 1, cuori = 2, fiori = 3 , picche = 4)
                Vector3 destination = Deposit[card.Seed-1].position;
                GameObject hintCard = Instantiate(card.gameObject, card.transform) as GameObject;
                hintCard.GetComponent<BoxCollider2D>().enabled = false;
                hintCard.transform.localPosition = Vector3.zero;
                hintCard.transform.SetParent(null);
                hintCard.transform.DOMove(destination, 0.5f);
                FadeColor(hintCard);
                Destroy(hintCard, 0.85f);
                hintFound = true;
                break;
            }

            if (card.Number == 13)
            {
                if (card.IsNextToDeck)
                    if (!card.IsFirst)
                        continue;

                for (int n = 0; n < TableBlock.Length; n++)
                {
                    Transform block = TableBlock[n];
                    if(block.GetComponent<TableBox>().IsFirst)
                    {
                        Vector3 destination = block.position;
                        GameObject hintCard = Instantiate(card.gameObject, card.transform) as GameObject;
                        hintCard.GetComponent<BoxCollider2D>().enabled = false;
                        hintCard.transform.localPosition = Vector3.zero;
                        hintCard.transform.SetParent(null);
                        hintCard.transform.DOMove(destination, 0.5f);
                        FadeColor(hintCard);
                        Destroy(hintCard, 0.85f);
                        hintFound = true;
                        break;
                    }
                }
            }

            LookForCard(card, numFacingUpCards, card.IsCardRed);

        }

        if (!hintFound)
        {
            if(DeckPos.transform.GetComponent<Deck>().AvailableCards != 0)
            {
                GameObject hintCard = Instantiate(CoveredCard, DeckPos) as GameObject;
                hintCard.transform.localPosition = Vector3.zero;
                hintCard.transform.SetParent(null);
                hintCard.transform.localPosition += Vector3.back * 499;

                hintCard.transform.DOMove(DeckSiderPos.position + Vector3.right * 0.5f, 0.5f);
                FadeColor(hintCard);
                Destroy(hintCard, 0.85f);
                hintFound = true;
            }

        }
    }

    private void LookForCard(Card _card, int _num, bool _cardRed)
    {
        for (int j = 0; j < _num; j++)
        {
            Card otherCard = FacingUpCards[j];

            if ((otherCard.IsCardRed != _cardRed && otherCard.IsFirst && otherCard.Number == _card.Number + 1 && !otherCard.IsNextToDeck && !otherCard.OnDeposit) ||
                (otherCard.OnDeposit && otherCard.Seed == _card.Seed && otherCard.Number == _card.Number - 1 && _card.IsFirst))
            {
                if(_card.transform.parent != null)
                {
                    int parentNumber = _card.transform.parent.GetComponent<Card>().Number;
                    if (otherCard.Number == parentNumber)
                        continue;
                }
                if (_card.IsNextToDeck && !_card.IsFirst)
                    continue;


                GameObject hintCard = Instantiate(_card.gameObject, _card.transform) as GameObject;
                hintCard.GetComponent<BoxCollider2D>().enabled = false;
                hintCard.transform.localPosition = Vector3.zero;
                hintCard.transform.SetParent(null);
                hintCard.transform.DOMove(otherCard.transform.position, 0.5f);
                FadeColor(hintCard);
                Destroy(hintCard, 0.85f);
                hintFound = true;
                break;
            }
        }
    }

    private void FadeColor(GameObject _go)
    {
        int childCount = _go.transform.childCount;
        if (childCount == 0)
            return;

        SpriteRenderer[] sprites = _go.GetComponentsInChildren<SpriteRenderer>();
        if (sprites.Length == 0)
            return;

        for (int i = 0; i < sprites.Length; i++)
        {
            sprites[i].sortingOrder = 500;


            sprites[i].DOFade(0, 0.5f);

        }

    }
}
