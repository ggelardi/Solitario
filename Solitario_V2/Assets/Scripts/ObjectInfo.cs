﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectInfo : MonoBehaviour {

    [HideInInspector]
    public ObjectInfo Instance;
    [HideInInspector]
    public Vector3 CurrentPos;
    [HideInInspector]
    public Transform CurrentParent;
    [HideInInspector]
    public Transform OldParent;
    [HideInInspector]
    public int OrderLayer;
    [HideInInspector]
    public bool WasInDeck;
    [HideInInspector]
    public List<Vector3> OldPositions = new List<Vector3>();


    [HideInInspector]
    public bool IsFirst = false;
    [HideInInspector]
    // La carta si trova nel mazzo
    public bool IsInDeck = true;
    [HideInInspector]
    // La carta è affiancata al mazzo
    public bool IsNextToDeck = false;
    [HideInInspector]
    // La carta si trova sul tavolo
    public bool OnTable = false;
    [HideInInspector]
    // La carta si trova nel deposito
    public bool OnDeposit = false;
    [HideInInspector]
    // La carta è rivolta verso l'alto
    public bool FacingUp = false;

    //Carta sottostante coperta
    public GameObject CoveredCard;

    [HideInInspector]
    public bool WasFirstOnTableBox = false;
    [HideInInspector]
    public TableBox OldTableBox;

    // Vogliamo anche registrare lo stato della vecchia carta parent
    // dato che quando faremo un undo dovrà tornare nel suo vecchio stato
    [HideInInspector]
    public bool OldParent_isFirst = false;
    [HideInInspector]
    public bool OldParent_IsInDeck = false;
    [HideInInspector]
    public bool OldParent_IsNextToDeck = false;
    [HideInInspector]
    public bool OldParent_OnTable = false;
    [HideInInspector]
    public bool OldParent_OnDeposit = false;
    [HideInInspector]
    public bool OldParent_FacingUp = false;


    private void Awake()
    {
        Instance = this;
    }

    private void OnEnable()
    {
        CardCreator.OnGameReady += CardCreator_OnGameReady;
    }
    private void OnDisable()
    {
        CardCreator.OnGameReady -= CardCreator_OnGameReady;
    }
    private void CardCreator_OnGameReady()
    {
        CurrentParent = this.transform.parent;
        CurrentPos = this.transform.position;
    }



}
