﻿using UnityEngine;

public class Deck : MonoBehaviour
{
    public GameObject[] AllCardsInDeck = new GameObject[52];

    public Sprite GameOverSprite;

    public bool EnableLimitedLifes = false;
    public int AvailableCards = 0;

    public int Lifes = 2;
    public DeckSider MyDeskSider;


    private void Start()
    {
        AvailableCards = 24;
    }

    void OnMouseDown()
    {
        print(AvailableCards);


        MyDeskSider.ResetCardInDeck();
        Counter.PointsCounter = 0;
        //    if (Lifes > 0)
        //    {
        //        AvailableCards = 0;
        //        Vector3 dsPos = MyDeskSider.transform.position;

        //        while (true)
        //        {
        //            RaycastHit2D hit;
        //            hit = Physics2D.Raycast(dsPos, Vector3.back);

        //            if (hit)
        //            {
        //                GameObject newCard = hit.collider.gameObject;
        //                Card card = newCard.GetComponent<Card>();

        //                Vector3 newPosition = new Vector3(transform.position.x, transform.position.y, -AvailableCards - 1);
        //                newCard.transform.position = newPosition;
        //                card.BackgroundSR.sortingOrder = AvailableCards;
        //                card.SeedSR.sortingOrder = AvailableCards + 1;
        //                card.NumberSR.sortingOrder = AvailableCards + 1;
        //                card.FacingUp = false;
        //                card.SetupCardApperance();
        //                card.IsInDeck = true;
        //                card.IsFirst = false;
        //                card.IsNextToDeck = false;

        //                AvailableCards++;
        //                MyDeskSider.CardsAbailable--;

        //                MyDeskSider.ResetCardInDeck();
        //                // MyDeskSider.CardsInDeckSider.Clear();
        //            }
        //            else
        //            {
        //                break;
        //            }
        //        }
        //        Counter.PointsCounter = 0;
        //        if(EnableLimitedLifes)
        //        Lifes--;
        //        if (Lifes == 0)
        //        {
        //            GetComponent<SpriteRenderer>().sprite = GameOverSprite;
        //        }
        //    }
    }
}
