﻿using UnityEngine;

public class TableBox : MonoBehaviour
{
    public bool IsFirst = false;
    public int CardsAvailable = 0;

    private BoxCollider2D myCollider;

    private void Start()
    {
        myCollider = this.GetComponent<BoxCollider2D>();
    }

    private void LateUpdate()
    {
        //Vogliamo sempre essere sicuri che il collider sia attivo
        if (!myCollider.enabled)
            myCollider.enabled = true;
    }
}
