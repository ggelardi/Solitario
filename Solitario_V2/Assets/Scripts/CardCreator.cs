﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardCreator : MonoBehaviour
{
    public BoxCollider2D BlockPanel;
    public GameObject Table;
    public float DistanceBlocks = 3f;
    public float VerticalOffsetHiddenCard = 0.2f;
    public float VerticalOffsetCards = 0.5f;
    public GameObject[] TableBlocks = new GameObject[7];
    public Transform[] Placeholders = new Transform[7];
    public Transform[] DepositBlocks = new Transform[4];
    public GameObject DepositBlockPrefab;
    public float DepositBlockDistances = 3f;
    public GameObject[] AllDepositBlocks = new GameObject[4];

    public delegate void CreatorEventHandler();
    public static event CreatorEventHandler OnGameReady;

    // Mazzo
    public Deck Deck;

    public List<GameObject> AllCards = new List<GameObject>();

    public float X_Offset = 3;
    public float Y_Offset = 4;

    public GameObject cardPrefab;

    public Sprite[] SeedsSprites = new Sprite[4];
    public Sprite[] RedFigures = new Sprite[3];
    public Sprite[] BlackFigures = new Sprite[3];


    public Sprite[] cardValueTexturesBlack = new Sprite[13];
    public Sprite[] cardValueTexturesRed = new Sprite[13];
    public HintSystem hintSystem;

    public static bool InitializationComplete = false;

    void Start()
    {
        WinGame.CoveredCards.Clear();

        BlockPanel.enabled = true;
        InitializationComplete = false;
        int allCardsIndex = 0;
        // Per ogni seme...
        for (int seedIndex = 0; seedIndex <= 3; seedIndex++)
        {
            // e per ogni numero di ogni seme ...
            for (int valueIndex = 0; valueIndex <= 12; valueIndex++)
            {
                // Genera una nuova carta rivolta verso il basso
                Vector3 newPosition = new Vector3(Deck.transform.position.x, Deck.transform.position.y, -Deck.AvailableCards);
                GameObject cardGO = Instantiate(cardPrefab, newPosition, Quaternion.identity);
                Card card = cardGO.GetComponent<Card>();
                card.FacingUp = false;

                // Assegna il seme corrispondente (1 = quadri, 2 = cuori, 3 = fiori, 4 = picche)
                card.Seed = seedIndex + 1;
                // Assegna il numero della carta (1 - K)
                card.Number = valueIndex + 1;
                // Assegna la sprite corrispondente 
                card.SeedSR.sprite = SeedsSprites[seedIndex];


                // Usa le figure per Jack,Queen e King se l'index corrisponde,
                // altrimenti usa seme normale
                if (valueIndex < 10)
                    card.CenterSR.sprite = SeedsSprites[seedIndex];
                else if (valueIndex == 10)
                {
                    if (seedIndex < 2)
                        card.CenterSR.sprite = RedFigures[0];
                    else
                        card.CenterSR.sprite = BlackFigures[0];
                }
                else if (valueIndex == 11)
                {
                    if (seedIndex < 2)
                        card.CenterSR.sprite = RedFigures[1];
                    else
                        card.CenterSR.sprite = BlackFigures[1];
                }
                else if (valueIndex == 12)
                {
                    if (seedIndex < 2)
                        card.CenterSR.sprite = RedFigures[2];
                    else
                        card.CenterSR.sprite = BlackFigures[2];
                }

                // Inizializza la carta
                card.SetupCardApperance();
                // in base al colore della carta assegna la sprite del numero col giusto colore
                if (card.IsCardRed)
                    card.NumberSR.sprite = cardValueTexturesRed[valueIndex];
                else
                    card.NumberSR.sprite = cardValueTexturesBlack[valueIndex];

                // Rinomina la carta 
                card.name = card.Number + " di " + card.SeedSR.sprite.name;

                // Aggiungi la carta nell'array che contiene tutte le carte
                AllCards[allCardsIndex] = card.gameObject;
                // Incrementa l'index
                allCardsIndex++;
            }
        }

        AllCards.ShuffleList();
        AllCards.ShuffleList();

        int[] allCardsIndexes = new int[52];
        for (int i = 0; i < allCardsIndexes.Length; i++)
        {
            allCardsIndexes[i] = i;
        }
        for (int i = 0; i < allCardsIndexes.Length; i++)
        {
            //Cache index
            int index = allCardsIndexes[i];
            int randomIndex = Random.Range(0, allCardsIndexes.Length - 1);
            allCardsIndexes[i] = allCardsIndexes[randomIndex];
            allCardsIndexes[randomIndex] = index;
        }

        for (int i = AllCards.Count - 1; i >= 0; i--)
        {
            GameObject cardGO = AllCards[allCardsIndexes[i]];
            Card card = cardGO.GetComponent<Card>();
            Deck.AllCardsInDeck[i] = cardGO;

            Vector3 newPosition = new Vector3(Deck.transform.position.x, Deck.transform.position.y, -Deck.AvailableCards - 1);
            cardGO.transform.position = newPosition;
            card.BackgroundSR.sortingOrder = Deck.AvailableCards;
            card.SeedSR.sortingOrder = Deck.AvailableCards + 1;
            card.CenterSR.sortingOrder = Deck.AvailableCards + 1;
            card.NumberSR.sortingOrder = Deck.AvailableCards + 1;


            Deck.AvailableCards++;
        }

        allCardsIndex = 0;
        
        StartCoroutine(Wait(0.1f, allCardsIndex));

        for (int depositBlockIndex = 0; depositBlockIndex < 4; depositBlockIndex++)
        {
            Vector3 pos = new Vector3(DepositBlocks[depositBlockIndex].position.x, DepositBlocks[depositBlockIndex].position.y, DepositBlockPrefab.transform.position.z);
            GameObject depositBlock = Instantiate(DepositBlockPrefab, pos, Quaternion.identity);
            depositBlock.GetComponent<DepositBox>().Seed = DepositBlocks[depositBlockIndex].GetComponent<SeedType>().MySeed;
            AllDepositBlocks[depositBlockIndex] = depositBlock;
        }

        hintSystem.AllCards.Clear();
        hintSystem.AllCards = AllCards;

    }

    IEnumerator Wait(float _seconds, int _allCardsIndex)
    {
        for (int tBlockIndex = 0; tBlockIndex < 7; tBlockIndex++)
        {
            Vector3 tableBlockNewPos = new Vector3(Placeholders[tBlockIndex].position.x, Placeholders[tBlockIndex].position.y, Table.transform.position.z);
            GameObject tableBlock = Instantiate(Table, tableBlockNewPos, Quaternion.identity);
            TableBlocks[tBlockIndex] = tableBlock;
            hintSystem.TableBlock[tBlockIndex] = tableBlock.transform;

            for (int cardsInTBlock = 0; cardsInTBlock < tBlockIndex + 1; cardsInTBlock++)
            {
                Card card = Deck.AllCardsInDeck[_allCardsIndex].GetComponent<Card>();

                TableBox block = tableBlock.GetComponent<TableBox>();

                card.BackgroundSR.sortingOrder = block.CardsAvailable;
                card.SeedSR.sortingOrder = block.CardsAvailable + 1;
                card.CenterSR.sortingOrder = block.CardsAvailable + 1;

                card.NumberSR.sortingOrder = block.CardsAvailable + 1;
                block.CardsAvailable++;
                card.OnTable = true;

                card.IsInDeck = false;

                Vector3 newPositionForCard;
                if (cardsInTBlock == tBlockIndex)
                {
                    card.FacingUp = true;
                    card.IsFirst = true;
                    card.SetupCardApperance();

                    newPositionForCard = new Vector3(block.transform.position.x, block.transform.position.y - cardsInTBlock * VerticalOffsetHiddenCard, -block.CardsAvailable);
                }
                else
                {
                    newPositionForCard = new Vector3(block.transform.position.x, block.transform.position.y - cardsInTBlock * VerticalOffsetHiddenCard, -block.CardsAvailable);
                    WinGame.CoveredCards.Add(card);
                }

                card.gameObject.transform.DOMove(newPositionForCard, 0.25f);
                _allCardsIndex++;
                yield return new WaitForSecondsRealtime(_seconds);
            }
        }

        InitializationComplete = true;
        BlockPanel.enabled = false;
        if (OnGameReady != null)
        {
            OnGameReady();
        }
    }
}
