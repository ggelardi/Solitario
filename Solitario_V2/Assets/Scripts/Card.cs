﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card : MonoBehaviour
{
    // La carta sta venendo spostata
    public bool IsBeingDragged = false;
    // La carta è la prima nella colonna
    public bool IsFirst = false;
    // La carta si trova nel mazzo
    public bool IsInDeck = true;
    // La carta è affiancata al mazzo
    public bool IsNextToDeck = false;
    // La carta si trova sul tavolo
    public bool OnTable = false;
    // La carta si trova nel deposito
    public bool OnDeposit = false;
    // La carta è rivolta verso l'alto
    public bool FacingUp = false;
    // La carta è rossa?
    public bool IsCardRed = false;
    // Valori da aggingere all'ordine del layer
    public int SortingOrderAddValue = 100;
    public int DepthZ = 100;
    // Valore seme (quadri = 1, cuori = 2, fiori = 3 , picche = 4)
    public int Seed = 0;
    public int Number = 0;
    // valori utilizzati per gestire la profondità delle sprite in Sprite Renderers (SR)
    public int BkGndSRSortingOrder;
    public int SeedSRSortingOrder;
    public int CenterSRSortingOrder;
    public int NumberSRSortingOrder;


    // La vecchia posizione della carta 
    private Vector3 OldCardPos;
    public Vector3 OldPosInCol;

    //Texture della carta in caso è coperta o scoperta
    public Sprite FrontCardTexture;
    public Sprite BackCardTexture;

    // Raccogli tutti gli SR della carta
    public SpriteRenderer BackgroundSR;
    public SpriteRenderer SeedSR;
    public SpriteRenderer CenterSR;
    public SpriteRenderer NumberSR;

    // Carta di appartenenza precedente
    private Transform OldParent;

    // Per risparmiare su cicly Update usiamo eventi quando qualcosa importante succede
    public delegate void CardEventHandler(GameObject _card, ObjectInfo _objectInfo);
    public static event CardEventHandler OnCardDropped;

    private KeyValuePair<GameObject, float> myGo = new KeyValuePair<GameObject, float>();
    private int oldSortingOrder;

    // Update is called once per frame
    void Update()
    {
        if (IsFirst && !FacingUp && OnTable)
        {
            print(this.name + " Needs to be flipped");
        }


        // Se la carta non viene più mossa e ha un "genitore" allora il genitore non saà più la prima carta
        // nella colonna
        if (!IsBeingDragged && transform.parent != null && transform.parent.GetComponent<Card>() != null)
        {
            Card cardParent = transform.parent.GetComponent<Card>();
            SpriteRenderer parentSR = cardParent.GetComponent<SpriteRenderer>();

            // Gestisci i layer della carta in modo tale che compaia sopra al genitore
            BackgroundSR.sortingOrder = parentSR.sortingOrder + 1;
            SeedSR.sortingOrder = BackgroundSR.sortingOrder + 1;
            NumberSR.sortingOrder = BackgroundSR.sortingOrder + 1;
            CenterSR.sortingOrder = BackgroundSR.sortingOrder + 1;

            //Il genitore non è più la prima carta
            cardParent.IsFirst = false;
        }
    }

    //I metodi OnMouse**** registrano gli eventi del mouse ma è compatibile con dispositivi mobile
    void OnMouseDrag()
    {
        if (ButtonsManager.GameInPause)
            return;

        IsBeingDragged = true;
        // Se la carta è scoperta e si trova su tavolo, deposito o affianco al mazzo
        // muovila seguendo il movimento del mose (dito nel caso mobile)
        if (FacingUp && (IsNextToDeck || OnTable || OnDeposit))
        {
            Vector3 mousePos = Camera.main.ScreenPointToRay(Input.mousePosition).origin + OldCardPos;
            this.transform.position = new Vector3(mousePos.x, mousePos.y, transform.position.z);
        }
    }


    void OnMouseDown()
    {
        if (ButtonsManager.GameInPause)
            return;

        // Registra la vecchia posizione della carta
        OldCardPos = this.transform.position - Camera.main.ScreenPointToRay(Input.mousePosition).origin;
        OldPosInCol = this.transform.position;

        // Al tap se la carta è coperta e si trova nel mazzo voltala 
        if (!FacingUp)
        {
            if (IsInDeck)
            {
                FacingUp = true;
                SetupCardApperance();
                float offsetDeckSider = 0f;
                myGo = new KeyValuePair<GameObject, float>(this.gameObject, OldPosInCol.z);
                //Trova il deck
                Deck deck = GameObject.Find("Deck").GetComponent<Deck>();
                deck.AvailableCards--;

                // trova l'oggetto deck sider (non sapevo come altro chiamarlo...fantasia da ingegnere)
                DeckSider deckSider = GameObject.Find("DeckSider").GetComponent<DeckSider>();
                int listLength = deckSider.CardsInDeckSider.Count;
                deckSider.CardsAbailable = listLength;
                if (listLength != 0)
                {
                    if (deckSider.CardsAbailable == 1)
                    {
                        offsetDeckSider = 0.25f;
                        deckSider.CardsInDeckSider[listLength - 1].Key.GetComponent<BoxCollider2D>().enabled = false;
                        deckSider.CardsInDeckSider[listLength - 1].Key.GetComponent<Card>().IsFirst = false;
                    }
                    else if (deckSider.CardsAbailable >= 2)
                    {
                        offsetDeckSider = 0.5f;
                        deckSider.CardsInDeckSider[listLength - 1].Key.GetComponent<BoxCollider2D>().enabled = false;
                        deckSider.CardsInDeckSider[listLength - 1].Key.GetComponent<Card>().IsFirst = false;
                    }
                }
                else
                    offsetDeckSider = 0;

                Vector3 newPos = new Vector3(deckSider.transform.position.x + offsetDeckSider, deckSider.transform.position.y, -deckSider.CardsAbailable);
                // Riorganizza i layers
                BackgroundSR.sortingOrder = deckSider.CardsAbailable * 2;
                SeedSR.sortingOrder = deckSider.CardsAbailable * 2 + 1;
                CenterSR.sortingOrder = deckSider.CardsAbailable * 2 + 1;
                NumberSR.sortingOrder = deckSider.CardsAbailable * 2 + 1;

                deckSider.ShiftCards();
                this.transform.DOMove(newPos, 0.5f).OnComplete(() =>
                  {
                      this.transform.position = newPos;
                  });

                // Incrementa il numero di carte affianco al mazzo
                deckSider.CardsAbailable++;
                deckSider.CardsInDeckSider.Add(myGo);
                // La carta diventa automaticamente la prima disponibile
                IsFirst = true;

                Counter.MoveCounter += 1;
                UndoStack.LastPointsAdded.Add(0);

                UpdateObjectInfo(true, false, null);
            }

            // Se la carta è coperta, è sul tavolo ed è la prima nella sua colonna girala
            if (OnTable && IsFirst)
            {
                FacingUp = true;
                SetupCardApperance();
                Counter.MoveCounter += 1;
            }
        }

        // Se la carta è scopetra e si trova in una delle tre postazioni
        if (FacingUp && (IsNextToDeck || OnTable || OnDeposit))
        {
            // Registra la posizione della carta nella colonna e l'attuale genitore
            OldPosInCol = this.transform.position;
            OldParent = this.transform.parent;
            // elimina i collegamenti
            this.transform.SetParent(null);

            oldSortingOrder = BackgroundSR.sortingOrder;
            BkGndSRSortingOrder = BackgroundSR.sortingOrder;
            SeedSRSortingOrder = SeedSR.sortingOrder;
            CenterSRSortingOrder = CenterSR.sortingOrder;
            NumberSRSortingOrder = NumberSR.sortingOrder;


            BackgroundSR.sortingOrder += SortingOrderAddValue;
            SeedSR.sortingOrder += SortingOrderAddValue;
            CenterSR.sortingOrder += SortingOrderAddValue;
            NumberSR.sortingOrder += SortingOrderAddValue;

            // Sposta in avanti la carta in modo che risulti sopra a tutte le altre carte sul tavolo
            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, -DepthZ);
        }
    }
    void OnMouseUp()
    {

        if (ButtonsManager.GameInPause)
            return;

        // La carta non viene più trascinata
        IsBeingDragged = false;

        if (FacingUp && !IsInDeck)// && (NextToDeck || OnTable || OnDeposit)
        {
            //Disabilita il collider
            BoxCollider2D boxCollider = GetComponent<BoxCollider2D>();
            boxCollider.enabled = false;

            DepositBox deposit = null;
            Card cardBelow = null;
            TableBox table = null;

            // Usa un raycaster per vedere cosa c'è sotto la carta
            RaycastHit2D hit;
            hit = Physics2D.Raycast(this.transform.position + (-OldCardPos), Vector3.forward);

            // Se c'è qualcosa registra le info dell'oggetto
            if (hit.collider != null)
            {
                cardBelow = hit.collider.GetComponent<Card>();
                table = hit.collider.GetComponent<TableBox>();
                deposit = hit.collider.GetComponent<DepositBox>();
            }
            // La carta selezionata è un blocco del deposito? questa carta è un asso? è prima?
            if (deposit != null && Number == 1 && IsFirst && Seed == (int)deposit.Seed)
            {
                PutCardOnDepositBox(hit, deposit);
            }
            // L'oggetto è il tavolo e non contiene altre carte? e questa carta è un K (l'unica che può essere messa sul tavolo) ?
            else if (table != null && table.IsFirst && Number == 13)
            {
                PutCardOnTableBox(hit, table);
            }
            // Sotto c'è una carta che si trova sul tavolo, è prima, rivolta verso l'alto, di valore superiore e di colore inverso?
            else if (cardBelow != null && cardBelow.OnTable && cardBelow.IsFirst && cardBelow.FacingUp && cardBelow.IsCardRed != IsCardRed && cardBelow.Number == Number + 1)
            {
                PutCardOnOtherCardOnTable(hit, cardBelow);
            }
            // Sotto c'è una carta che è prima nel deposito, è dello stesso seme, è rivolta verso l'alto ed è di valore inferiore
            else if (cardBelow != null && IsFirst && cardBelow.OnDeposit && cardBelow.IsFirst && cardBelow.FacingUp && cardBelow.Seed == Seed && cardBelow.Number == Number - 1)
            {
                PutCardOnOtherCardInDeposit(hit, cardBelow);
            }
            // Se non è nel mazzo semplicemente riportala dov'era prima di muoverla
            else if (!IsInDeck)
            {
                // Riporta carta in poszione originale
                this.transform.position = OldPosInCol;
                this.transform.SetParent(OldParent);
                // Resetta gli SR
                BackgroundSR.sortingOrder = BkGndSRSortingOrder;
                SeedSR.sortingOrder = SeedSRSortingOrder;
                CenterSR.sortingOrder = SeedSRSortingOrder;
                NumberSR.sortingOrder = NumberSRSortingOrder;

                hit = new RaycastHit2D();
            }
            // Se la carta ha cambiato posizione resetta le informazioni della carta o del box che ha lasciato
            if (hit.collider != null && (table != null || deposit != null || cardBelow != null))
            {
                RaycastHit2D hitCardBelow;
                hitCardBelow = Physics2D.Raycast(OldPosInCol, Vector3.forward);
                Card cardBlw = null;
                TableBox tableBelow = null;
                DepositBox depositBelow = null;

                if (hitCardBelow.collider != null)
                {
                    cardBlw = hitCardBelow.collider.GetComponent<Card>();
                    tableBelow = hitCardBelow.collider.GetComponent<TableBox>();
                    depositBelow = hitCardBelow.collider.GetComponent<DepositBox>();
                }
                if (cardBlw != null)
                {
                    UndoStack.CoveredCards.Add(cardBlw);

                    cardBlw.IsFirst = true;
                    cardBlw.FacingUp = true;
                    cardBlw.SetupCardApperance();

                    if (WinGame.CoveredCards.Contains(cardBlw))
                        WinGame.CoveredCards.Remove(cardBlw);
                }
                else if (depositBelow != null)
                {
                    depositBelow.IsFirst = true;
                }
                else if (tableBelow != null)
                {
                    tableBelow.IsFirst = true;
                }

            }
            // Riabilita il box collider di questa carta
            boxCollider.enabled = true;
        }
        // Se era nel deck registra che è stata rimossa
        if (IsInDeck)
        {
            IsNextToDeck = true;
            IsInDeck = false;
        }
    }

    /// <summary>
    /// Determina se la carta è rossa o nera gestisci le sprite
    /// </summary>
    public void SetupCardApperance()
    {
        // Determina colore della carta
        if (Seed < 3)
            IsCardRed = true;
        else
            IsCardRed = false;

        // Se la carta è rivolta verso l'alto attiva tutti i componenti visivi
        // altrimenti disattivali e usa l'immagine del retro
        if (FacingUp)
        {
            BackgroundSR.sprite = FrontCardTexture;
            SeedSR.gameObject.SetActive(true);
            CenterSR.gameObject.SetActive(true);
            NumberSR.gameObject.SetActive(true);
        }
        else
        {
            BackgroundSR.sprite = BackCardTexture;
            SeedSR.gameObject.SetActive(false);
            CenterSR.gameObject.SetActive(false);
            NumberSR.gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Posiziona carta sul tavolo
    /// </summary>
    /// <param name="_hit"></param>
    /// <param name="_table"></param>
    private void PutCardOnTableBox(RaycastHit2D _hit, TableBox _table)
    {
        // Posiziona la carta sopra al blocco del tavolo
        this.transform.position = new Vector3(_hit.collider.transform.position.x, _hit.collider.transform.position.y, _hit.collider.transform.position.z - 1);
        // Pulisci la gerarchia della carta
        this.transform.SetParent(null);

        SpriteRenderer tableSR = _table.GetComponent<SpriteRenderer>();
        // Assicurati di posizionare i layers della carta in modo che risulti sopra al blocco del tavolo
        BackgroundSR.sortingOrder = tableSR.sortingOrder + 1;
        SeedSR.sortingOrder = BackgroundSR.sortingOrder + 1;
        CenterSR.sortingOrder = BackgroundSR.sortingOrder + 1;
        NumberSR.sortingOrder = BackgroundSR.sortingOrder + 1;
        // La carta è sicuramente la prima in cima
        IsFirst = true;
        _table.IsFirst = false;

        if (IsNextToDeck)
        {
            DeckSider deckSider = GameObject.Find("DeckSider").GetComponent<DeckSider>();
            deckSider.CardsAbailable--;
            deckSider.CardsInDeckSider.Remove(myGo);

            deckSider.ResetLastCardCollider();
        }


        // E non si trova da nessun altra parte se non sul tavolo
        OnTable = true;
        OnDeposit = false;
        IsNextToDeck = false;

        UpdateObjectInfo(false, true, _table);
        Counter.MoveCounter += 1;
        Counter.PointsCounter += 5;
        UndoStack.LastPointsAdded.Add(10);
    }

    /// <summary>
    /// Posiziona carta su deposito
    /// </summary>
    /// <param name="_hit"></param>
    /// <param name="_depositBox"></param>
    private void PutCardOnDepositBox(RaycastHit2D _hit, DepositBox _depositBox)
    {
        transform.position = new Vector3(_hit.collider.transform.position.x, _hit.collider.transform.position.y, _hit.collider.transform.position.z - 1);
        transform.SetParent(null);

        SpriteRenderer cell4SR = _depositBox.GetComponent<SpriteRenderer>();
        BackgroundSR.sortingOrder = cell4SR.sortingOrder + 1;
        SeedSR.sortingOrder = BackgroundSR.sortingOrder + 1;
        CenterSR.sortingOrder = BackgroundSR.sortingOrder + 1;

        NumberSR.sortingOrder = BackgroundSR.sortingOrder + 1;

        _depositBox.IsFirst = false;
        IsFirst = true;

        if (IsNextToDeck)
        {
            DeckSider deckSider = GameObject.Find("DeckSider").GetComponent<DeckSider>();
            deckSider.CardsAbailable--;
            deckSider.CardsInDeckSider.Remove(myGo);

            deckSider.ResetLastCardCollider();
        }

        OnTable = false;
        IsNextToDeck = false;
        OnDeposit = true;

        UpdateObjectInfo(false, false, null);

        Counter.MoveCounter += 1;
        Counter.PointsCounter += 10;
        UndoStack.LastPointsAdded.Add(10);
    }
    /// <summary>
    /// Posiiziona questa carta su un'altra carta che si trova sul tavolo
    /// </summary>
    /// <param name="_hit"></param>
    /// <param name="_otherCard"></param>
    private void PutCardOnOtherCardOnTable(RaycastHit2D _hit, Card _otherCard)
    {
        // Posiziona la carta con un offset sull'asse y
        this.transform.position = new Vector3(_hit.collider.transform.position.x,
            _hit.collider.transform.position.y - FindObjectOfType<CardCreator>().VerticalOffsetCards,
            _hit.collider.transform.position.z - 1);
        // Inseriscila nella gerarchia della carta su cui viene depositata
        this.transform.SetParent(_hit.collider.transform);
        // Gestisci gli SR
        BackgroundSR.sortingOrder = _otherCard.BackgroundSR.sortingOrder + 1;
        SeedSR.sortingOrder = _otherCard.SeedSR.sortingOrder + 1;
        CenterSR.sortingOrder = _otherCard.CenterSR.sortingOrder + 1;
        NumberSR.sortingOrder = _otherCard.NumberSR.sortingOrder + 1;
        // La carta sottostante non è più la prima nella colonna
        _otherCard.IsFirst = false;
        // Se questa carta non ha altri figli nella gerarchia è la prima nella colonna
        if (this.transform.childCount < 3)
            IsFirst = true;

        if (IsNextToDeck)
        {
            DeckSider deckSider = GameObject.Find("DeckSider").GetComponent<DeckSider>();
            deckSider.CardsAbailable--;
            deckSider.CardsInDeckSider.Remove(myGo);

            deckSider.ResetLastCardCollider();
        }

        // La carta è sul tavolo
        OnTable = true;
        OnDeposit = false;
        IsNextToDeck = false;

        UpdateObjectInfo(false, false, null);

        Counter.MoveCounter += 1;
        Counter.PointsCounter += 5;
        UndoStack.LastPointsAdded.Add(5);
    }

    /// <summary>
    /// Posiziona carta su altra carta nel deposito
    /// </summary>
    /// <param name="_hit"></param>
    /// <param name="_cardBelow"></param>
    private void PutCardOnOtherCardInDeposit(RaycastHit2D _hit, Card _cardBelow)
    {
        transform.position = new Vector3(_hit.collider.transform.position.x, _hit.collider.transform.position.y, _hit.collider.transform.position.z - 1);
        transform.SetParent(null);

        BackgroundSR.sortingOrder = _cardBelow.BackgroundSR.sortingOrder + 1;
        SeedSR.sortingOrder = BackgroundSR.sortingOrder + 1;
        CenterSR.sortingOrder = BackgroundSR.sortingOrder + 1;
        NumberSR.sortingOrder = BackgroundSR.sortingOrder + 1;

        _cardBelow.IsFirst = false;
        IsFirst = true;

        if (IsNextToDeck)
        {
            DeckSider deckSider = GameObject.Find("DeckSider").GetComponent<DeckSider>();
            deckSider.CardsAbailable--;
            deckSider.CardsInDeckSider.Remove(myGo);

            deckSider.ResetLastCardCollider();
        }

        OnTable = false;
        IsNextToDeck = false;
        OnDeposit = true;
        UpdateObjectInfo(false, false, null);
        Counter.PointsCounter += 10;
        UndoStack.LastPointsAdded.Add(10);
    }

    private void UpdateObjectInfo(bool _wasInDeck, bool _wasFirstOnTableBox, TableBox _table)
    {
        ObjectInfo objInfo = this.GetComponent<ObjectInfo>().Instance;

        objInfo.CurrentParent = this.transform.parent;
        objInfo.CurrentPos = this.transform.position;
        objInfo.OldParent = OldParent;
        objInfo.OldPositions.Add(OldPosInCol);
        objInfo.IsFirst = IsFirst;
        objInfo.IsInDeck = IsInDeck;
        objInfo.IsNextToDeck = IsNextToDeck;
        objInfo.OnDeposit = OnDeposit;
        objInfo.OnTable = OnTable;
        objInfo.OrderLayer = oldSortingOrder;
        objInfo.WasInDeck = _wasInDeck;
        objInfo.WasFirstOnTableBox = _wasFirstOnTableBox;
        objInfo.OldTableBox = _table;

        // Registra info riguardanti il vecchio parent così che da poterle
        // resettare in caso di un undo
        if (OldParent != null)
        {
            Card oldParentCard = OldParent.GetComponent<Card>();
            objInfo.OldParent_FacingUp = oldParentCard.FacingUp;
            objInfo.OldParent_isFirst = oldParentCard.IsFirst;
            objInfo.OldParent_IsInDeck = oldParentCard.IsInDeck;
            objInfo.OldParent_IsNextToDeck = oldParentCard.IsNextToDeck;
            objInfo.OldParent_OnDeposit = oldParentCard.OnDeposit;
            objInfo.OldParent_OnTable = oldParentCard.OnTable;
        }

        // Attiva l'evento per segnalare che la carta è stata posizionata
        if (OnCardDropped != null)
            OnCardDropped(this.gameObject, objInfo);
    }
}
