﻿using UnityEngine;

public class DepositBox : MonoBehaviour
{
    public bool IsFirst = false;
    public int CardsAvailable = 0;
    public SeedTypes Seed;
}
