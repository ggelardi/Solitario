﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButtonsManager : MonoBehaviour {

    public static bool GameInPause = false;
    public GameObject Menu;
    public GameObject PausePanel;

    private void Start()
    {
        PausePanel.GetComponent<Image>().enabled = false;
        Menu.SetActive(false);
        GameInPause = false;
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(1);
    }

    public void QuitApplication()
    {
        Application.Quit();
    }

    public void OpenMenu()
    {
        PausePanel.GetComponent<Image>().enabled = true;
        Menu.SetActive(true);
        GameInPause = true;
    }

    public void CloseMenu()
    {
        PausePanel.GetComponent<Image>().enabled = false;
        Menu.SetActive(false);
        GameInPause = false;
    }
}
