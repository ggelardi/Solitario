﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum SeedTypes
{
    Quadri = 1,
    Cuori = 2,
    Fiori = 3,
    Picche = 4

}

public class SeedType : MonoBehaviour {

    public SeedTypes MySeed;
}
