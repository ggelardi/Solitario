﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinGame : MonoBehaviour {

    public static List<Card> CoveredCards = new List<Card>();
    public static bool GameWon = false;
    public BoxCollider2D BlockPanel;
    public GameObject WinPanel;
    public Text PointsText;
    public int CoveredCardsNum = 0;

    private void Start()
    {
        GameWon = false;
    }

    // Update is called once per frame
    void Update () {

        CoveredCardsNum = CoveredCards.Count;

        if (CardCreator.InitializationComplete)
        {
            if (CoveredCards.Count == 0)
            {
                BlockPanel.enabled = true;
                WinPanel.SetActive(true);
                PointsText.text = "PUNTI: " + Counter.PointsCounter;
            }
        }


	}
}
