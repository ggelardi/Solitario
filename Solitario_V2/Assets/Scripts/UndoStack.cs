﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UndoStack : MonoBehaviour
{

    public static List<KeyValuePair<GameObject, ObjectInfo>> StackList = new List<KeyValuePair<GameObject, ObjectInfo>>();
    public static List<Card> CoveredCards = new List<Card>();
    public static List<int> LastPointsAdded = new List<int>();
    private bool skipSection = false;

    private void Start()
    {
        CoveredCards.Clear();
        StackList.Clear();
        LastPointsAdded.Clear();
    }

    private void OnEnable()
    {
        Card.OnCardDropped += Card_OnCardDropped;
    }

    private void OnDisable()
    {
        Card.OnCardDropped -= Card_OnCardDropped;
    }

    private void Card_OnCardDropped(GameObject _card, ObjectInfo _objectInfo)
    {
        StackList.Add(new KeyValuePair<GameObject, ObjectInfo>(_card, _objectInfo));
    }

    public void UndoAction()
    {
        skipSection = false;
        int listCount = StackList.Count;
        //List di carte coperte
        int ccList = CoveredCards.Count;

        if (listCount == 0)
            return;

        // Ottieni il GameObject
        GameObject go = StackList[listCount - 1].Key;

        // Se la carta aveva un parent, resetta il parent alle condizioni precedenti
        if (StackList[listCount - 1].Value.Instance.OldParent != null)
        {
            Card oldParentCard = StackList[listCount - 1].Value.Instance.OldParent.GetComponent<Card>();

            oldParentCard.IsFirst = StackList[listCount - 1].Value.Instance.OldParent_isFirst;
            oldParentCard.IsInDeck = StackList[listCount - 1].Value.Instance.OldParent_IsInDeck;
            oldParentCard.IsNextToDeck = StackList[listCount - 1].Value.Instance.OldParent_IsNextToDeck;
            oldParentCard.FacingUp = StackList[listCount - 1].Value.Instance.FacingUp;
            oldParentCard.OnDeposit = StackList[listCount - 1].Value.Instance.OnDeposit;
            oldParentCard.OnTable = StackList[listCount - 1].Value.Instance.OnTable;
            oldParentCard.SetupCardApperance();
        }
        // Se la carta dispone di un parent in questo momento, resetta le 
        // impostazioni dell'attuale parent a com'erano precedentemente
        if (go.transform.parent != null)
        {
            Card currentParent = go.transform.parent.GetComponent<Card>();
            currentParent.IsFirst = true;
            currentParent.IsInDeck = false;
            currentParent.IsNextToDeck = false;
            currentParent.FacingUp = true;
            currentParent.OnDeposit = false;
            currentParent.OnTable = true;
            currentParent.SetupCardApperance();
        }

        // Cache il componente
        Card goCard = go.GetComponent<Card>();
        // Rimuovi parent, riposiziona, resetta parent, reset collider e layers
        go.transform.SetParent(null);
        int numOldPositions = StackList[listCount - 1].Value.Instance.OldPositions.Count;
        go.transform.DOMove(StackList[listCount - 1].Value.Instance.OldPositions[numOldPositions - 1], 0.3f);
        StackList[listCount - 1].Value.Instance.OldPositions.RemoveAt(numOldPositions - 1);
        go.transform.SetParent(StackList[listCount - 1].Value.Instance.OldParent);
        go.GetComponent<BoxCollider2D>().enabled = true;
        goCard.BackgroundSR.sortingOrder = StackList[listCount - 1].Value.Instance.OrderLayer;
        goCard.SeedSR.sortingOrder = goCard.BackgroundSR.sortingOrder + 1;
        goCard.CenterSR.sortingOrder = goCard.BackgroundSR.sortingOrder + 1;
        goCard.NumberSR.sortingOrder = goCard.BackgroundSR.sortingOrder + 1;
        DeckSider deckSider = GameObject.Find("DeckSider").GetComponent<DeckSider>();

        if (go.GetComponent<ObjectInfo>().Instance.WasInDeck)
        {
            goCard.IsInDeck = true;
            goCard.IsFirst = false;
            goCard.OnTable = false;
            goCard.FacingUp = false;
            goCard.OnDeposit = false;
            goCard.IsNextToDeck = false;
            int elementsInDeckSider = deckSider.CardsInDeckSider.Count;
            deckSider.CardsInDeckSider.RemoveAt(elementsInDeckSider - 1);
            deckSider.ResetLastCardCollider();
            goCard.SetupCardApperance();
            skipSection = true;
        }
        // Se vi erano carte coperte...
        if (ccList != 0 && !skipSection)
        {
            // (cahce la carta)
            Card coveredCard = CoveredCards[ccList - 1];
            // ...reimpostale in base a dove si trovavano precedentemente
            if (!coveredCard.IsNextToDeck)
            {
                coveredCard.FacingUp = false;
                coveredCard.IsFirst = false;
                coveredCard.IsNextToDeck = false;
                coveredCard.OnTable = true;
                coveredCard.OnDeposit = false;
                coveredCard.IsInDeck = false;
                // Resetta l'aspetto della carta
                coveredCard.SetupCardApperance();
            }
            else
            {
                coveredCard.FacingUp = true;
                coveredCard.IsFirst = false;
                coveredCard.OnTable = false;
                coveredCard.OnDeposit = false;
                coveredCard.IsInDeck = false;
                // Resetta il collider
                coveredCard.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            }
            if (coveredCard.OnTable)
                WinGame.CoveredCards.Add(coveredCard);

            // Rimuovi la carta dalla lista
            CoveredCards.RemoveAt(ccList - 1);
        }
        if (StackList[listCount - 1].Value.Instance.WasFirstOnTableBox)
        {
            if (StackList[listCount - 1].Value.Instance.OldTableBox != null)
            {
                StackList[listCount - 1].Value.Instance.OldTableBox.CardsAvailable = 0;
                StackList[listCount - 1].Value.Instance.OldTableBox.IsFirst = true;
            }

        }

        Counter.MoveCounter += 1;
        int numPointsAdd = LastPointsAdded.Count;
        Counter.PointsCounter -= LastPointsAdded[numPointsAdd - 1];
        LastPointsAdded.RemoveAt(numPointsAdd - 1);
        StackList.RemoveAt(listCount - 1);

    }
}
