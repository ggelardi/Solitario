﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Counter : MonoBehaviour {

    // Contatori per mnumero di mosse fatte e punti accumulati
    public static int MoveCounter = 0;
    public static int PointsCounter = 0;

    public Text Mosse;
    public Text Punti;

    public static bool Draw3 = false;
	// Use this for initialization
	void Start () {
        MoveCounter = 0;
        PointsCounter = 0;
	}

    void Update () {

        Mosse.text = "MOSSE\n" + MoveCounter;
        Punti.text = "PUNTI\n" + PointsCounter;
	}
}
